# Miqueias Vitor Lopes Lima
# NUSP 11796340

using LinearAlgebra

function multiplica(a, b)
  dima = size(a)
  dimb = size(b)
   if dima[2] != dimb[1]
     return -1
   end
  c = zeros(dima[1], dimb[2])
    for i in 1:dima[1]
      for j in 1:dimb[2]
        for k in 1:dima[2]
          c[i, j] = c[i, j] + a[i, k] * b[k, j]
        end
      end
    end
  return c
end

function matrix_pot(M, p)
    if p == 1
        return M
    end
    j = M
    while p > 1
        M = multiplica(M, j)
        p = p - 1
    end
    return M
end

function matrix_pot_by_squaring(M, p)
    #= essa função transforma p em binário e adiciona cada 
    algarismo a um vetor v e esse vetor é usado durante o método
    básico de exponenciação =#

    if p == 1
        return M
    end
    
    v = []      
    while p > 0
        n = p % 2
        push!(v, n)
        p = p ÷ 2
    end
    
    i = length(v)
    r = 1 * Matrix(I, size(M)[1], size(M)[2])
    while i > 1
        if v[i] == 1
           r = multiplica(r, M)
        end
           r = multiplica(r, r)
        i = i - 1
    end

    if v[1] == 0
        return r
    else
        return multiplica(r, M)
    end
end


using Test

function test()
    @test matrix_pot([1 2; 3 4], 2) == convert(Array{Float64}, [7 10; 15 22])
    @test matrix_pot_by_squaring([1 2; 3 4], 2) == convert(Array{Float64}, [7 10; 15 22])
    @test matrix_pot([1 2 0; 4 0 9; 13 11 12], 7) == matrix_pot_by_squaring([1 2 0; 4 0 9; 13 11 12], 7)
    @test matrix_pot([2 3 4 5 6; 2 3 4 5 6; 2 3 4 5 6; 2 3 4 5 6; 2 3 4 5 6], 7) == 
    matrix_pot_by_squaring([2 3 4 5 6; 2 3 4 5 6; 2 3 4 5 6; 2 3 4 5 6; 2 3 4 5 6], 7) 
    @test matrix_pot([0 0; 0 0], 7) == matrix_pot_by_squaring([0 0; 0 0], 7)
    println("Fim dos testes")
end

   
function compare_times()
   M = Matrix(I, 30, 30)

  @time matrix_pot(M, 10)
  @time matrix_pot_by_squaring(M, 10)

end


test()
compare_times()

